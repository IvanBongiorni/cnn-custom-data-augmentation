"""
Author: Ivan Bongiorni
2020-04-27

Pre-processing pipeline for Kaggle's 10 Monkey Species dataset []
It must be run from terminal to get ready-to-train, processed dataset.
"""

def _move_files(source,  destination):
    ''' Moves all files listed from one directory to another '''
    import os
    import shutil

    file_list = os.listdir(source)
    for file in file_list:
        shutil.move(source + file, destination)
    return None


def organize_dataset(source_path, destination_path, params):
    ''' Preprocessing step, to be called for both Train and Test data. '''
    import os
    import time
    import numpy as np
    import pandas as pd

    start = time.time()

    ### STEP 1.
    # Extract labeled dataset of image names
    # get list of files in folder
    list_alouatta = os.listdir(source_path + 'n0/')
    list_eryth = os.listdir(source_path + 'n1/')
    list_cacajo = os.listdir(source_path + 'n2/')
    list_macaca = os.listdir(source_path + 'n3/')
    list_cebuella = os.listdir(source_path + 'n4/')
    list_cebus = os.listdir(source_path + 'n5/')
    list_mico = os.listdir(source_path + 'n6/')
    list_saimiri = os.listdir(source_path + 'n7/')
    list_aotus = os.listdir(source_path + 'n8/')
    list_trachy = os.listdir(source_path + 'n9/')

    # repeat name of classes for all files in the lists
    label_alouatta = ['alouatta_palliata'] * len(list_alouatta)
    label_eryth = ['erythrocebus_patas'] * len(list_eryth)
    label_cacajo = ['cacajao_calvus'] * len(list_cacajo)
    label_macaca = ['macaca_fuscata'] * len(list_macaca)
    label_cebuella = ['cebuella_pygmea'] * len(list_cebuella)
    label_cebus = ['cebus_capucinus'] * len(list_cebus)
    label_mico = ['mico_argentatus'] * len(list_mico)
    label_saimiri = ['saimiri_sciureus'] * len(list_saimiri)
    label_aotus = ['aotus_nigriceps'] * len(list_aotus)
    label_trachy = ['trachypithecus_johnii'] * len(list_trachy)

    # create list of file names and labels
    image_names = list_alouatta + list_eryth + list_cacajo + list_macaca + list_cebuella + list_cebus + list_mico + list_saimiri + list_aotus + list_trachy
    train_labels = label_alouatta + label_eryth + label_cacajo + label_macaca + label_cebuella + label_cebus + label_mico + label_saimiri + label_aotus + label_trachy
    train_labels = pd.Series(train_labels)

    print('Creating labels dataset.')
    train_labels = pd.get_dummies(train_labels)  # one-hot encode labels and attach filenames
    train_labels['image_names'] = image_names
    train_labels = shuffle(train_labels, random_state = params['random_seed'])  # Shuffle before training
    train_labels.to_csv(os.getcwd() + '/data/training/' + 'train_labels.csv', index = False)

    ### STEP 2.
    print('Moving files to destination folder...')
    destination_path = os.getcwd() + '/data/training/Train/'
    _move_files(source_path + 'n0/', destination_path)
    _move_files(source_path + 'n1/', destination_path)
    _move_files(source_path + 'n2/', destination_path)
    _move_files(source_path + 'n3/', destination_path)
    _move_files(source_path + 'n4/', destination_path)
    _move_files(source_path + 'n5/', destination_path)
    _move_files(source_path + 'n6/', destination_path)
    _move_files(source_path + 'n7/', destination_path)
    _move_files(source_path + 'n8/', destination_path)
    _move_files(source_path + 'n9/', destination_path)

    ### STEP 3.
    print('Scaling image size to specified range.')
    for image in image_names:
        image = plt.imread(destination_path + image)
        image = tf.image.resize_with_pad(image,
                                         target_height = params['image_resize'],
                                         target_width = params['image_resize'])
        plt.imsave(path, destination_path + image)

    print('Data preprocessing executed in {} seconds.'.format(round(time.time()-start, 2)))
    return None


def main():
    '''
    For both Train and Test data:
    1. Generate a dataframe containing all image filenames, associated
        with a pandas DataFrame that pairs filnames and one-hot encoded labels
    2. Moves all images into the same folder.
    3. Scales image size to specified range.
    '''
    import time
    import os
    import yaml
    import numpy as np
    import pandas as pd
    from sklearn.utils import shuffle

    start = time.time()

    # Read params
    params = yaml.load(open(os.getcwd(), '/config.yaml'), yaml.Loader)

    print('\nProcessing Train data:')
    source_path = os.getcwd() + '/data_raw/training/training/'
    destination_path = os.getcwd() + '/data_processed/Training/'
    organize_dataset(source_path, destination_path, params)

    print('\nProcessing Test data:')
    source_path = os.getcwd() + '/data_raw/validation/validation/'
    destination_path = os.getcwd() + '/data_processed/training/Test/'
    organize_dataset(source_path, destination_path, params)

    return None


if __name__ == '__main__':
    main()
