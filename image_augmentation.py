"""
Author: Ivan Bongiorni
2020-04-27

Genrator of Image Augmentation Schema

This script exists to make it easies to manipulate Image Augmentation.
It is based on the **albumentations** library. It generates a schema for augmentation
that can be called later in the pipeline and applied to each image.
"""

def create_augmentation_schema(params):
    import albumentations
    from albumentations import (
        Compose, OneOf, Rotate, ShiftScaleRotate, RandomCrop,
        HorizontalFlip, RandomBrightnessContrast
    )

    augmentation_schema = Compose([
        OneOf([
                Rotate(),
                ShiftScaleRotate(shift_limit = params['shift_limit'],
                                 scale_limit = params['scale_limit'],
                                 rotate_limit = params['rotate_limit']
              )], p = params['p_shiftscalerotate']),

        #RandomCrop(height = int(height*0.7), width = int(width*0.7), p = 0.5),

        HorizontalFlip(p = params['p_horizontal_flip']),

        RandomBrightnessContrast(brightness_limit = params['brightness_limit'],
                                 contrast_limit = params['contrast_limit'],
                                 p = params['p_randombrightnesscontrast'])

        ], p = params['p_augmentation']
    )
    return augmentation_schema
