"""
Author: Ivan Bongiorni
2020-04-28

Model implementation and training.
"""

def build(params):
    '''
    This CNN is implemented with two separate Convolutional blocks. The first is
    much simpler, with a lower number of filters, it is meant to capture the
    simplest signals from data. The second is deeper and with more filters, it is
    meant to elaborate complex abstracions.
    Their concatenation to Dense layers is meant to implement a kind of 'skip
    connection' (like a much simpler version of a ResNet).
    '''
    from tensorflow.keras.models import Model
    from tensorflow.keras.layers import (
        Input, Conv2D, MaxPooling2D, Flatten, Concatenate,
        Dense, Dropout, BatchNormalization
        )

    input = Input((height, width, 3))

    ### FIRST CONV BLOCK
    conv_1 = Conv2D(filters = params['conv_filters'][0],
                    kernel_size = params['kernel_size'][0],
                    strides = params['stride'][0],
                    activation = params['conv_activation'])(input)
    if params['conv_batchnorm'][0]:
        conv_1 = BatchNormalization()(conv_1)
    if params['conv_dropout'][0]:
        conv_1 = Dropout(params['conv_dropout'][0])(conv_1)

    conv_2 = Conv2D(filters = params['conv_filters'][1],
                    kernel_size = params['kernel_size'][0],
                    strides = params['stride'][0],
                    activation = params['conv_activation'])(conv_1)
    if params['conv_batchnorm']:
        conv_2 = BatchNormalization()(conv_2)
    if params['conv_dropout']:
        conv_2 = Dropout(params['conv_dropout'][0])(conv_2)

    maxpool_1 = MaxPooling2D(pool_size = (2, 2))(conv_2)

    ### SECOND CONV BLOCK
    conv_3 = Conv2D(filters = params['conv_filters'][2],
                    kernel_size = params['kernel_size'][1],
                    strides = params['stride'][1],
                    activation = params['conv_activation'])(input)
    if params['conv_batchnorm']:
        conv_3 = BatchNormalization()(conv_3)

    conv_4 = Conv2D(filters = params['conv_filters'][3],
                    kernel_size = params['kernel_size'][1],
                    strides = params['stride'][1],
                    activation = params['conv_activation'])(conv_3)
    if params['conv_batchnorm']:
        conv_4 = BatchNormalization()(conv_4)

    conv_5 = Conv2D(filters = params['conv_filters'][4],
                    kernel_size = params['kernel_size'][1],
                    strides = params['stride'][1],
                    activation = params['conv_activation'])(conv_4)
    if params['conv_batchnorm']:
        conv_5 = BatchNormalization()(conv_5)

    maxpool_2 = MaxPooling2D(pool_size = (2, 2))(conv_5)

    # Flatten and Concatenate signals
    flat_1 = Flatten()(maxpool_1)
    concatenation = Concatenate()([flat_1, flat_2])

    ### DENSE BLOCK
    dense_1 = Dense(params['dense_size'],
                    activation = params['dense_activation'])(concatenation)
    if params['dense_batchnorm']:
        dense_1 = BatchNormalization()(dense_1)
    if params['dense_dropout']:
        dense_1 = Dropout(params['dense_dropout'])(dense_1)

    dense_2 = Dense(params['dense_size'],
                    activation = params['dense_activation'])(dense_1)

    output = Dense(10, activation = softmax)

    model = Model(inputs = [input], outputs = [output])
    return model


def _fetch_images(image_names, train_labels, iteration, path_to_data, augmentation_schema, params):
    '''
    Extracts batch of filenames, loads corresponding files as np.array's
    Increase the size of the dataset, and performs augmentation of each element.
    Called from train().
    '''
    from matplotlib import pyplot as plt

    def augment_image(image, augmentation_schema = augmentation_schema):
        image = augmentation_schema(image = image)['image']
        image = image / 255.
        image = image * 2 - 1
        return image

    # Take array of filenames that compose the batch and corresponding one-hot encoded labels matrix
    take = iteration * params['batch_size']
    image_names_batch = image_names[take:take+params['batch_size']]
    train_labels_batch = train_labels[ take:take+params['batch_size'] , : ]

    # Load images to np.array's, multiply by expansion factor, and augment them
    #image_batch = [ load_image(name) for name in image_names_batch ]
    image_batch = [ plt.imread('{}/{}'.format(path_to_data, name)) for name in image_names_batch ]
    image_batch = [image_batch] * params['expansion']
    train_labels_batch = [train_labels_batch] * params['expansion']
    image_batch = [ augment_image(image) for image in image_batch ]

    image_batch = np.concatenate(image_batch)
    train_labels_batch = np.concatenate(train_labels_batch)

    image_batch = image_batch.astype(np.float32)
    train_labels_batch = train_labels_batch.astype(np.float32)
    return image_batch, train_labels_batch


def train(CNN, params):
    '''

    [ spiega come hai condotto l'indicizzazione ]

    [ spiega come hai implementato l'early stopping ]

    '''
    import os
    import time
    import numpy as np
    import tensorflow as tf

    ### TODO: RECUPERA TUTTI QUESTI ELEMENTI
    image_names
    train_labels
    augmentation_schema
    path_to_data = os.getcwd() + ''   ### TODO: AGGIUNGI PATH CORRETTO

    index = np.array(range(len(imamge_names)))

    print('\nTraining start.\n')

    optimizer = tf.keras.optimizers.Adam(learning_rate = params['learning_rate'])
    loss = tf.keras.losses.CategoricalCrossentropy()

    @tf.function
    def train_model_on_batch():
        with tf.GradientTape() as tape:
            current_loss = tf.reduce_mean(loss(CNN(X_batch), Y_batch))
        gradients = tape.gradient(current_loss, CNN.trainable_variables)
        optimizer.apply_gradients(zip(gradients, CNN.trainable_variables))
        return current_loss

    ## Canonical training
    for epoch in range(params['n_epochs']):
        start = time.time()

        if params['shuffle']:
            ### TODO: CAMBIA QUESTA PARTE, DEVI FARE LO SHUFFLE DELLE LISTE
            index = np.random.choice(len(index), size = len(index), replace = False)

        for iteration in range(X_train.shape[0] // params['batch_size']):
            X_batch, Y_batch = _fetch_images(image_names = image_names,
                                             train_labels = train_labels,
                                             iteration = iteration,
                                             path_to_data = path_to_data,
                                             augmentation_schema = augmentation_schema,
                                             params = params)
            current_loss = train_model_on_batch()

        ### TODO: TAKE VALIDATION LOSS HERE

        print('{}.{}   \tTraining Loss: {}    \tValidation Loss: {}    \tTime: {}ss'.format(
            epoch, iteration, current_loss.numpy(), validation_loss.numpy(), round(time.time()-start, 2)
        ))

    # if params['patience']:
    #    ### TODO: ACTIVATE EARLY STOPPING
    #    best_loss = current_loss
    #    stopping_counter = 0
    #
    #    while stopping_counter < params['patience']:
    #        ### TODO: STUDIA UN MODO PER REITERARE SULLE IMMAGINI
    #        current_loss = train_model_on_batch()
    #        if current_loss > best_loss:
    #            stopping_counter += 1
    #        else:
    #            ### TODO: SAVE MODEL CHECKPOINT
    #            best_loss = current_loss
    #            stopping_counter = 0
    #    print('Early stopping training')

    print('\nTraining complete.')

    model.save('{}/saved_models/{}.h5'.format(os.getcwd(), params['model_name']))
    print('\nModel saved as:\n{}'.format('{}/saved_models/{}.h5'.format(os.getcwd(), params['model_name'])))

    return None


def check_performance_on_test_data(in_notebook = False):
    '''
    Loads a pretrained model and checks its performance on test data.

    Args:
    in_notebook: whether performance metrics must be analyzed from a Jupyter Notebook
        or simply displayed in terminal.
    '''
    import os
    import numpy as np
    import tensorflow as tf
    # import sklearn confusion matrix and all the metrics

    # Load config params

    # Load test data


    # Load model and predict
    CNN = tf.keras.models.load_model( os.getcwd() + '/saved_models/' + params['model_name'] + '.h5' )
    P = CNN.predict(T)

    # Get Confusion Matrix and performance metrics

    if in_notebook:
        metrics = {
            'confusion_matrix': CM,
            'precision': precision,
            'recall', recall,
            'f1_score':, f1_score
        }
        return metrics
    else:
        print(CM)

        print('\nMetrics:')
        print('Accuracy:  ', accuracy)
        print('Precision: ', precision)
        print('Recall:    ', recall)
        print('F1:        ', f1_score)
        return None
